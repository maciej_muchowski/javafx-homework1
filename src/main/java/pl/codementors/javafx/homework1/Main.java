package pl.codementors.javafx.homework1;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;

public class Main extends Application {
    public void start(Stage stage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene(root, 600, 600, Color.BLACK);

        Double[] points = {205.0, 150.0, 217.0, 186.0, 259.0, 186.0,
                223.0, 204.0, 233.0, 246.0, 205.0, 222.0, 177.0, 246.0, 187.0, 204.0,
                151.0, 186.0, 193.0, 186.0};
        Polygon star = new Polygon();
        star.getPoints().addAll(points);
        star.setFill(Color.BLUE);
        star.setLayoutX(100);
        star.setLayoutY(100);
        root.getChildren().add(star);

        Text text = new Text("Homeworks are awesome");
        text.setFill(Color.GREEN);
        text.setX(235);
        text.setY(150);
        text.setScaleX(2);
        text.setScaleY(2);
        root.getChildren().add(text);

        Random random = new Random();
        double x = 0, y = 0;
        for (int i = 0; i < 23; i++) {
            if (i < 12) {
                if (i % 2 == 0) {
                    drawRectangle(x, y, random, root);
                    drawCircle(x + 25, y + 550, random, root);
                    x += 75;
                } else {
                    drawCircle(x, y, random, root);
                    drawRectangle(x - 25, y + 550, random, root);
                    x += 25;
                }
            } else {
                x = 0;
                if (i % 2 == 0) {
                    y += 50;
                    drawRectangle(x + 550, y, random, root);
                    x += 25;
                    drawCircle(x, y, random, root);
                } else {
                    y += 50;
                    drawRectangle(x, y, random, root);
                    x += 25;
                    drawCircle(x + 550, y, random, root);
                }
            }
        }
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static void drawRectangle(double x, double y, Random random, Group root) {
        Rectangle rectangle = new Rectangle(25 * 2 ^ 1 / 2, 25 * 2 ^ 1 / 2,
                Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble()));
        rectangle.setX(x);
        rectangle.setY(y);
        root.getChildren().add(rectangle);
    }

    private static void drawCircle(double x, double y, Random random, Group root) {
        Circle circle = new Circle(25,
                Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble()));
        circle.setCenterX(x);
        circle.setCenterY(y + 25);
        root.getChildren().add(circle);

    }
}
